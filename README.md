# Repositorios total de servicios

- Principal:                    https://gitlab.com/bravogermanezequiel/progdist2_starter
- Servicio Libreria:            https://gitlab.com/bravogermanezequiel/progdist2_serviciolibreria
- Servicio Ordenes:             https://gitlab.com/bravogermanezequiel/progdist2_serviciogeneradorordenes
- Servicio Envios:              https://gitlab.com/bravogermanezequiel/progdist2_sistemaenvios
- Sistema/Middleware Libreria:  https://gitlab.com/bravogermanezequiel/progdist2_sistemalibreria
- Serv. Consultas Gerenciales:  https://gitlab.com/bravogermanezequiel/servicioconsultasgerenciales

# Requisitos

- Tener instalado:
    - Para SO's Linux:      [Docker](https://www.docker.com/)
    - Para Windows/macOS:   [Docker Dekstop](https://www.docker.com/products/docker-desktop)
    - Docker-compose:       [Link](https://docs.docker.com/compose/install/#install-compose-on-linux-systems)

# Instrucciones para correr el proyecto

**Los procesos de arranque del resto de los servicios estan centralizados en este unico repositorio**
```
$ git clone https://gitlab.com/bravogermanezequiel/progdist2_starter
$ cd progdist2_starter
$ docker-compose up
```

**Al correr el proyecto deberiamos tener los siguientes contenedores levantados:**
```
CONTAINER ID   IMAGE                                                  COMMAND                  CREATED        STATUS                       PORTS                                                                                                                                                 NAMES
dd5c08c76da0   prom/prometheus:v2.1.0                                 "/bin/prometheus --c…"   45 hours ago   Up About an hour             0.0.0.0:9090->9090/tcp, :::9090->9090/tcp                                                                                                             prometheus
3502b286893b   grafana/grafana:7.1.5                                  "/run.sh"                45 hours ago   Up About an hour             0.0.0.0:3600->3000/tcp, :::3600->3000/tcp                                                                                                             grafana
cc495b86be0e   progdist2_starter_filebeat                             "/usr/bin/tini -- /u…"   45 hours ago   Up About an hour                                                                                                                                                                   fb01
e92b71f07d06   progdist2_starter_metricbeat                           "/usr/bin/tini -- /u…"   45 hours ago   Up About an hour                                                                                                                                                                   mb01
fe80333206db   progdist2_starter_nginx                                "/docker-entrypoint.…"   45 hours ago   Up About an hour (healthy)   0.0.0.0:80->80/tcp, :::80->80/tcp                                                                                                                     nginx
fc3c28c1852b   docker.elastic.co/elasticsearch/elasticsearch:7.16.0   "/bin/tini -- /usr/l…"   45 hours ago   Up About an hour (healthy)   0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 9300/tcp                                                                                                   es01
855810f951b8   docker.elastic.co/kibana/kibana:7.16.0                 "/bin/tini -- /usr/l…"   45 hours ago   Up About an hour (healthy)   0.0.0.0:5601->5601/tcp, :::5601->5601/tcp                                                                                                             kib01
afc4ab56566c   progdist2_starter_sistema_libreria                     "python sistema.py"      45 hours ago   Up About an hour                                                                                                                                                                   progdist2_starter_sistema_libreria_1
91f6344a2634   progdist2_starter_consultas_gerenciales                "python server.py"       45 hours ago   Up About an hour (healthy)                                                                                                                                                         progdist2_starter_consultas_gerenciales_1
20a461e8fb65   progdist2_starter_envios                               "python server.py"       45 hours ago   Up About an hour (healthy)                                                                                                                                                         progdist2_starter_envios_1
284b371d93ab   progdist2_starter_ordenes                              "python server.py"       45 hours ago   Up About an hour (healthy)                                                                                                                                                         progdist2_starter_ordenes_1
7c9110d48eca   progdist2_starter_libreria                             "python server.py"       45 hours ago   Up 21 seconds (healthy)                                                                                                                                                            progdist2_starter_libreria_1
aee207c8afa9   rabbitmq:3-management-alpine                           "docker-entrypoint.s…"   46 hours ago   Up About an hour (healthy)   4369/tcp, 5671/tcp, 0.0.0.0:5672->5672/tcp, :::5672->5672/tcp, 15671/tcp, 15691-15692/tcp, 25672/tcp, 0.0.0.0:15672->15672/tcp, :::15672->15672/tcp   progdist2_starter_rabbitmq_1


```

## Escalamiento

**En el caso de querer escalar algun servicio, de forma horizontal se puede disponer sobre los siguiente**
- ordenes
- envios
- libreria
- consultas_gerenciales

Se puede realizar de la siguiente manera, escalamos a dos instancias libreria y ordenes, los demas servicios quedan con una sola instancia
```
$docker-compose up -d --scale libreria=2 --scale ordenes=2
```
Podemos verificar que se crean dos contenedores de los servicios mencionados:
```
$docker ps

CONTAINER ID   IMAGE                                                  COMMAND                  CREATED          STATUS                    PORTS                                                                                                                                                 NAMES
55df7d6d1a09   progdist2_starter_ordenes                              "python server.py"       14 seconds ago   Up 13 seconds (healthy)                                                                                                                                                         progdist2_starter_ordenes_2
a93918495925   progdist2_starter_libreria                             "python server.py"       20 seconds ago   Up 18 seconds (healthy)                                                                                                                                                         progdist2_starter_libreria_2
04d751acace0   prom/prometheus:v2.1.0                                 "/bin/prometheus --c…"   3 hours ago      Up 2 hours                0.0.0.0:9090->9090/tcp, :::9090->9090/tcp                                                                                                             prometheus
a2a4c7d5ce54   grafana/grafana:7.1.5                                  "/run.sh"                3 hours ago      Up 2 hours                0.0.0.0:3600->3000/tcp, :::3600->3000/tcp                                                                                                             grafana
94a107561c77   progdist2_starter_filebeat                             "/usr/bin/tini -- /u…"   3 hours ago      Up 2 hours                                                                                                                                                                      fb01
1e01c441fac2   progdist2_starter_nginx                                "/docker-entrypoint.…"   3 hours ago      Up 2 hours (healthy)      0.0.0.0:80->80/tcp, :::80->80/tcp                                                                                                                     nginx
6a84ed84f0c2   progdist2_starter_metricbeat                           "/usr/bin/tini -- /u…"   3 hours ago      Up 2 hours                                                                                                                                                                      mb01
26fe610e75aa   docker.elastic.co/kibana/kibana:7.16.0                 "/bin/tini -- /usr/l…"   3 hours ago      Up 2 hours (healthy)      0.0.0.0:5601->5601/tcp, :::5601->5601/tcp                                                                                                             kib01
89bb70dcd46c   docker.elastic.co/elasticsearch/elasticsearch:7.16.0   "/bin/tini -- /usr/l…"   3 hours ago      Up 2 hours (healthy)      0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 9300/tcp                                                                                                   es01
4cbb33543534   progdist2_starter_consultas_gerenciales                "python server.py"       3 hours ago      Up 2 hours (healthy)                                                                                                                                                            progdist2_starter_consultas_gerenciales_1
5e1be931d7de   progdist2_starter_sistema_libreria                     "python sistema.py"      3 hours ago      Up 2 hours                                                                                                                                                                      progdist2_starter_sistema_libreria_1
ac5f66c407e4   progdist2_starter_envios                               "python server.py"       3 hours ago      Up 2 hours (healthy)                                                                                                                                                            progdist2_starter_envios_1
17c1bd096664   progdist2_starter_ordenes                              "python server.py"       3 hours ago      Up 2 hours (healthy)                                                                                                                                                            progdist2_starter_ordenes_1
d33e2fcbec2a   progdist2_starter_libreria                             "python server.py"       3 hours ago      Up 2 hours (healthy)                                                                                                                                                            progdist2_starter_libreria_1
248b0bc00743   rabbitmq:3-management-alpine                           "docker-entrypoint.s…"   3 hours ago      Up 2 hours (healthy)      4369/tcp, 5671/tcp, 0.0.0.0:5672->5672/tcp, :::5672->5672/tcp, 15671/tcp, 15691-15692/tcp, 25672/tcp, 0.0.0.0:15672->15672/tcp, :::15672->15672/tcp   progdist2_starter_rabbitmq_1

```
# Decisiones de diseño

## Módulos

La aplicación se divide en 5 servicios:
- Servicio de colas: es el encargado de suministrar un servicio de cola AMQP con RabbitMQ. El mismo sirve para poder procesar ordenes a medida que se van generando con la posibilidad de ser reencoladas y procesadas nuevamente en caso de falla o indisponibilidad de servicio consumidor del exchange.
- Libreria: servicio que expone los endpoints y operaciones necesarias para administrar libros y sus dependencias como autores, editoriales, generos y socios. Permite crear, listar, modificar, obtener y eliminar los mencionados anteriormente.
- Ordenes: servicio que expone los endpoints y operaciones necesarias para generar ordenes, listarlas y modificarlas. Es el proceso que dispara el procesamiento de las mismas para poder generar un envio.
- Envios: servicio que expone los endpoints y operaciones necesarias para crear, modificar, listar y eliminar envios así como transportes, en conjunto con sus disponibilidades, etc.
- Middleware: servicio que se encarga de consumir el exchange asociado a la cola de ordenes e interactua con el resto de los servicios para generar envios de acorde a disponibilidades de transportes, volumenes libres, días, etc.
- Consultas gerenciales: es el encargado de suministrar reportes asociados a consultas gerenciales de todos los servicios, así como socio con mayor envios, libro y genero mas vendido.

## Comunicación

La comunicación entre los módulos es practicamente asíncrona pues de esta manera para la generación de ordenes y su respectivo procesamiento para obtener un envio como resultado se realiza de forma asíncrona por medio de una cola de mensajes, lo que desacopla ambos servicios y permite a los servicios que interactuan con el usuario en la generación de ordenes continuar con sus operaciones sin un bloqueante algúno.
Para la comunicación entre los procesos se decidio utilizar REST para la comunicación entre servicios de API HTTP y AMQP haciendo uso de RabbitMQ para la comunicación con el servicio de colas de mensajeria.

### Patrones a cumplir

El mensaje lo único que debe respetar es ser un JSON con un campo _**id**_, unica condición necesaria. Del resto debe contar con un delivery tag apropiado para poder realizar su ACK (acknowledgement) o NACK de forma adecuada en caso de ser procesarse correctamente o no.

### Problematicas por elección de arquitectura

- Una de las problemáticas mas evidente, es la complejidad del propio código y de la arquitectura en sí, pues se debio implementar en principio un servicio de cola AMQP, además de tener que handlear los posibles casos donde no existan transportes, por ejemplo. Ahora bien, cabe aclara que esta decisión tambien trae sus ventajas, pues no es necesario esperar desde el lado del cliente a que este proceso termine para poder continuar con sus operaciones, pues el mensaje de orden queda en la cola a ser procesado cuando haya disponibilidad en el sistema.
- Otra problemática que se puede presentar es que se sabe que los mensajes no se asegura que lleguen en orden, aunque para este caso de una unica cola no es un problema.

### Posibles lineas de acción a futuro:

- Implementar un dead letter exchange para atrapar mensajes que por algún motivo quedan en un estado de re encolamiento infinito, de tal manera que por un vencimiento de TTL del mensaje o algun tipo de envenamiento del mismo, se termine llevando a esa cola.

# Ejemplos de uso

Una vez inicializados todos los servicios y corriendo:

1. Se deben agregar socios. Para ello se debe realizar una petición a la siguiente ruta:

Dataset de socios de ejemplo:
```
#Socio 1
{
    "nombre": "Francisco",
    "apellido": "Lincoln",
    "edad": 50
}

#Socio 2
{
    "nombre": "Juan",
    "apellido": "Percoln",
    "edad": 31
}

#Socio 3
{
    "nombre": "Bart",
    "apellido": "Simpson",
    "edad": 20
}
```

(POST) http://localhost/libreria/socio
![](/imagenes/createSocio.png)

Como se observa, el cuerpo debe contener tales atributos con los valores de nombre, apellido y edad adecuados.

2. Se deben cargar libros. Para ello se debe realizar una petición a la siguiente ruta:

Dataset de libros de ejemplo:
```
# Libro 1
{ 
    "titulo": "Hansel y Gretel", 
    "autorId": 1, 
    "generoId": 2, 
    "editorialId": 1, 
    "volumen": 2 
}

# Libro 2
{ 
    "titulo": "Rocky IV", 
    "autorId": 2, 
    "generoId": 1, 
    "editorialId": 1, 
    "volumen": 4 
}

# Libro 3
{ 
    "titulo": "Caperucita Roja", 
    "autorId": 3, 
    "generoId": 3, 
    "editorialId": 1, 
    "volumen": 2 
}
```

(POST) http://localhost/libreria/libro
![](/imagenes/createLibro.png)

Como se observa, el cuerpo debe contener tales atributos con los valores de autorId, generoId, editorialId adecuados, pues es requisito que estos id para las entidades existan previamente.

3. Se debe crear al menos un transporte. Para ello se debe realizar una petición a la siguiente ruta:

Dataset de transportes de ejemplo:
```
# Transporte 1
{ 
    "patente": "AB123CD",
    "volumen": 50,
    "disponibilidadesHorarias": [
        [0, "08:00", "17:00"],
        [1, "08:00", "18:00"],
        [2, "08:00", "18:00"],
        [3, "15:00", "20:00"]
    ]
}

# Transporte 2
{ 
    "patente": "CD123CE",
    "volumen": 40,
    "disponibilidadesHorarias": [
        [0, "08:00", "17:00"],
        [1, "08:00", "18:00"],
        [2, "08:00", "18:00"],
        [3, "15:00", "20:00"],
        [4, "15:00", "20:00"],
        [5, "15:00", "20:00"],
        [6, "15:00", "20:00"]
    ]
}
```

(POST) http://localhost/envios/transporte
![](/imagenes/createTransporte.png)

Como se observa, el cuerpo debe contener una patente no existente ya en el sistema, un volumen asociado a la capacidad del vehiculo para llevar libros y sus disponibilidades horarias, donde 0 = Lunes, 1 = Martes ... 6 = Domingo, seguido de los horarios.

4. Luego se puede crear una orden. Los pasos anteriores son requisitos al menos una vez para que existan libros (lo que nos permitira crear la orden), un transporte (necesario para que el sistema pueda crear un envio asociando la orden al transportista) y un socio (para poder asociarlo como generador de la orden)

Dataset de ordenes de ejemplo:
```
# Orden 1
{
    "librosCantidad": [
        [1,5],
        [2,10]
    ],
    "socioId": 1,
    "direccion": "Avenida Corrientes 1232 - Capital Federal"
}

# Orden 2
{
    "librosCantidad": [
        [1,2],
        [2,1],
        [3,4]
    ],
    "socioId": 2,
    "direccion": "California 1100 - Capital Federal"
}

# Orden 3
{
    "librosCantidad": [
        [2,1]
    ],
    "socioId": 3,
    "direccion": "Avenida Corrientes 1100 - Capital Federal"
}
```

(POST) http://localhost/ordenes/orden
![](/imagenes/createOrden.png)

Como se observa, se debe pasar un valor "librosCantidad" que acepta un arreglo de arreglos, donde para cada sub arreglo la primera posición hace referencia al id del libro que se añade a la orden y la segunda posición refiere a la cantidad de libros para ese id especifico que se añaden a la orden.
Luego a modo de especificación se añade una dirección destino para la entrega.
Se debe prestar suma atención al campo "id" que nos devuelve en la respuesta en caso de que se haya creado la orden pues lo usaremos luego.

5. Luego con el "id" de la orden generada puedo consultar el estado para saber si se me ha asignado un envio adecuado para mi orden o no:

(GET) http://localhost/ordenes/orden/{id}
![](/imagenes/getOrden.png)

Donde se puede observar dentro de ellos dos campos importantes:
- procesado: campo booleano que nos aportada un indicio de si nuestra orden fue procesada y asignada a un envio
- envioId: id del envio que se nos ha asignado

6. Con el "id" del envio que se nos ha asignado anteriormente luego de procesada nuestra orden podemos consultar los datos de nuestro envio como la fecha, que otras ordenes se estarán enviando junto a la mia y la patente del vehiculo asignado:

(GET) http://localhost/envios/envio/{id}
![](/imagenes/getEnvio.png)

7. Por ultimo y para finalizar es posible realizar consultas gerenciales sobre las siguientes rutas:
- Obtener un reporte del libro mas vendido en el mes y año otorgados por parámetro. Debemos pasar un "año" y un "mes" adecuados:

GET http://localhost/reporte/libro/{int:anio}/{int:mes}
![](/imagenes/getLibroMasVendidoMes.png)
- Obtener un reporte del socio que mayor cantidad de envios se le ha realizado en la totalidad del tiempo.

GET http://localhost/reporte/socio/mayorEnvios
![](/imagenes/getSocioMayorEnvios.png)
- Obtener un reporte del genero mas vendido en el mes y año otorgados por parámetro. Debemos pasar un "año" y un "mes" adecuados:

GET http://localhost/reporte/genero/{int:anio}/{int:mes}
![](/imagenes/getGeneroMasVendidoMes.png)
